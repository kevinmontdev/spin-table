package mx.com.ito.spintable.app.main.presenter;

import mx.com.ito.spintable.app.main.MainPresenterContract;
import mx.com.ito.spintable.app.main.MainViewContract;
import mx.com.ito.spintable.models.CError;
import mx.com.ito.spintable.models.CUser;
import mx.com.ito.spintable.repository.UserRepository;

public class MainPresenter implements MainPresenterContract {

    // dependencies
    private MainViewContract view;

    public MainPresenter(MainViewContract view) {
        this.view = view;
    }

    public void doLogin(CUser credential) {
        CUser temporaryCredential = UserRepository.getUserTemporyStub();
        if (temporaryCredential.getUser().equals(credential.getUser())) {
            if (temporaryCredential.getPassword().equals(credential.getPassword())) {
                // view navigate to next view
                view.nextView();
            } else {
                // show on password error
                view.showError(new CError("Contraseña incorrecta", "Login"));
            }
        } else {
            //show on error user
            view.showError(new CError("Usuario incorrecto", "Login"));
        }
    }
}

