package mx.com.ito.spintable.repository;

import mx.com.ito.spintable.models.CUser;

public class UserRepository {

    public static CUser getUserTemporyStub() {
        CUser temporaryStub = new CUser();
        temporaryStub.setUser("MesaRotBlue");
        temporaryStub.setPassword("crodeorizaba");
        return temporaryStub;
    }
}
