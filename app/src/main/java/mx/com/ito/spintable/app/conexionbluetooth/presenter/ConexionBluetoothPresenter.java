package mx.com.ito.spintable.app.conexionbluetooth.presenter;

import mx.com.ito.spintable.app.conexionbluetooth.ConexionBluetoothPresenterContract;
import mx.com.ito.spintable.app.conexionbluetooth.ConexionBluetoothViewContract;

public class ConexionBluetoothPresenter implements ConexionBluetoothPresenterContract {

    // class dependencies
    private ConexionBluetoothViewContract view;

    public ConexionBluetoothPresenter(ConexionBluetoothViewContract view) {
        this.view = view;
    }
}
