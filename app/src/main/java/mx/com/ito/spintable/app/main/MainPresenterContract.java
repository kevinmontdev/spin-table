package mx.com.ito.spintable.app.main;

import mx.com.ito.spintable.models.CUser;

public interface MainPresenterContract {

    void doLogin(CUser credential);

}
