package mx.com.ito.spintable.app.panel.presenter;

import mx.com.ito.spintable.app.panel.PanelPresenterContract;
import mx.com.ito.spintable.app.panel.PanelViewContract;

public class PanelPresenter implements PanelPresenterContract {

    PanelViewContract view;

    public PanelPresenter(PanelViewContract view) {
        this.view = view;
    }
}
