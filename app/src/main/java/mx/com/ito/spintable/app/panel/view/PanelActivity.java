package mx.com.ito.spintable.app.panel.view;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import mx.com.ito.spintable.app.conexionbluetooth.view.ConexionBluetoothActivity;
import mx.com.ito.spintable.R;
import mx.com.ito.spintable.app.main.view.MainActivity;
import mx.com.ito.spintable.app.panel.PanelViewContract;
import mx.com.ito.spintable.app.panel.PanelPresenterContract;
import mx.com.ito.spintable.app.panel.presenter.PanelPresenter;

public class PanelActivity extends AppCompatActivity implements PanelViewContract {

    // Class dependencies
    private PanelPresenterContract presenter;

    // View Variables
    private Spinner opciones,velocidad,temperatura;
    private ImageButton regresar,increment,decrement;
    private Button inicar,stop,conectar,desactivar;
    private TextView contador;

    // Class variables
    private Chronometer cronometro;
    private BluetoothDevice mDevice = null;

    private int cont=0;

    //Variables para Bluethoot
    BluetoothAdapter mBlueAdapter;

    private static final int REQUEST_ENABLE_BT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.presenter = new PanelPresenter(this);
        setContentView(R.layout.panel);

        //Obtener variable
        opciones = findViewById(R.id.giro);
        velocidad = findViewById(R.id.velocidad);
        temperatura = findViewById(R.id.temperatura);
        regresar = findViewById(R.id.regresar_inicio);
        inicar = findViewById(R.id.Play);
        cronometro = findViewById(R.id.cronometro);
        stop = findViewById(R.id.Stop);
        increment = findViewById(R.id.increment);
        decrement = findViewById(R.id.decrement);
        contador = findViewById(R.id.contador);
        conectar = findViewById(R.id.conectar);
        desactivar = findViewById(R.id.desconectar);

        desactivar.setEnabled(false);
        //Array de datos
        final String[] giros =
                new String[]{"Izquierda","Derecha"};
        final String[] velocidadades =
                new String[]{"1x","2x","3x"};
        final String[] temperaturas =
                new String[]{"Calido"};

        //Se crea el Adaptador
        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, giros);
        ArrayAdapter<String> adaptador2 =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, velocidadades);
        ArrayAdapter<String> adaptador3 =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, temperaturas);

        //lanzando adpatador
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        opciones.setAdapter(adaptador);
        velocidad.setAdapter(adaptador2);
        temperatura.setAdapter(adaptador3);


        //Seleccion de la Opcion
        opciones.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id) {
                        //mensaje.setText("Seleccionado: " +parent.getItemAtPosition(position));
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                        //mensaje.setText("");
                    }
                });


        //Regreso al Inicio
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //cronometro iniciado
        inicar.setEnabled(true);
        inicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mDevice == null) {
                    showToast("Ningun dispositivo bluetooth ha sido conectado");
                    return;
                }

                // checar mDevice si el dispositivo ha sido conectado

                stop.setVisibility(View.VISIBLE);
                inicar.setVisibility(View.INVISIBLE);
                cronometro.setBase(SystemClock.elapsedRealtime());
                cronometro.start();
                desactivar.setVisibility(View.INVISIBLE);
            }
        });

        //cronometro detenido

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inicar.setEnabled(true);
                stop.setVisibility(View.INVISIBLE);
                inicar.setVisibility(View.VISIBLE);
                cronometro.stop();
                desactivar.setVisibility(View.VISIBLE);
            }
        });


        //Incremento en Y
        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 cont++;
                contador.setText(""+cont);
            }
        });


        //Decremento en Y
        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cont--;
                contador.setText(""+cont);
            }
        });


        //Bluethoot

        //Adapter


        //on btn click
        conectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(PanelActivity.this, ConexionBluetoothActivity.class);
                startActivityForResult(intent2, 2);
            }
        });

        //Desactivar Bluethoot
        desactivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBlueAdapter.isEnabled()) {
                    mBlueAdapter.disable();
                    desactivar.setEnabled(false);
                    showToast("Turning Bluetooth Off");
                } else {
                    showToast("Bluetooth is already off");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode){
            case 2:
                showToast("Recuperar el dipositivo bluetooth");
                // conectarlo automaticamente
                // habilitar el boton de desconectar bluetooth
                break;
            case 0:
                Log.e("Error","error en el resultado");
                showToast("could't connect bluetooth");
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
