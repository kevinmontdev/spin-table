package mx.com.ito.spintable.models;

import androidx.annotation.NonNull;

public class CUser {

    private String user;
    private String password;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @NonNull
    @Override
    public String toString() {
        return "<CUser: [user: "+ user +", password: "+ password+"]>";
    }
}
