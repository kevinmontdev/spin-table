package mx.com.ito.spintable.app.main;

import mx.com.ito.spintable.models.CError;

public interface MainViewContract {

    void showError(CError error);

    void nextView();
}
