package mx.com.ito.spintable.app.conexionbluetooth.view;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import mx.com.ito.spintable.R;
import mx.com.ito.spintable.app.conexionbluetooth.ConexionBluetoothPresenterContract;
import mx.com.ito.spintable.app.conexionbluetooth.ConexionBluetoothViewContract;
import mx.com.ito.spintable.app.conexionbluetooth.presenter.ConexionBluetoothPresenter;

public class ConexionBluetoothActivity extends AppCompatActivity implements ConexionBluetoothViewContract {

    // Class dependencies
    private ConexionBluetoothPresenterContract presenter = null;

    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 1;
    Button buscar;
    private TextView vinculado,datos;
    BluetoothAdapter mBlueAdapter;
    ListView lista;
    //Array para guardar los dispositivos
    private ArrayList<String> MAC;
    private ArrayAdapter<String> adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.presenter = new ConexionBluetoothPresenter(this);
        setContentView(R.layout.conexion_bluethoot);

        datos = findViewById(R.id.dispositivosencontrados);
        buscar = findViewById(R.id.buscar);
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBlueAdapter.isEnabled()){
            showToast("Turning On Bluetooth...");
            //intent to on bluetooth
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        } else {
            Intent intent = new Intent();
            setResult(0, intent);
            finish();
        }


        //agregando datos al array
        MAC=new ArrayList<String>();
        MAC.add("marcos : 43734843");
        MAC.add("luis : 6554343");

        //Se guarda en el adaptador del ListView
        adaptador=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,MAC);
        lista=(ListView)findViewById(R.id.lista);
        lista.setAdapter(adaptador);


        //List seleccionable
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ConexionBluetoothActivity.this, ""+ MAC.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK){
                    //bluetooth is on
                    showToast("Bluetooth is on");
                }  else {
                    //user denied to turn bluetooth on
                    Intent intent = new Intent();
                    setResult(0, intent);
                    finish();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }



}
