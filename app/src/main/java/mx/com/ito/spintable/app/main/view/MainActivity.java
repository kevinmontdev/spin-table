package mx.com.ito.spintable.app.main.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.com.ito.spintable.app.panel.view.PanelActivity;
import mx.com.ito.spintable.R;
import mx.com.ito.spintable.app.main.MainPresenterContract;
import mx.com.ito.spintable.app.main.MainViewContract;
import mx.com.ito.spintable.app.main.presenter.MainPresenter;
import mx.com.ito.spintable.models.CError;
import mx.com.ito.spintable.models.CUser;

public class MainActivity extends AppCompatActivity implements MainViewContract {

    private Button mEntrarButton;
    private EditText mUserEditText;
    private EditText mPasswordEditText;

    //presenter
    private MainPresenterContract presenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MainPresenter(this);

        setContentView(R.layout.activity_main);

        mEntrarButton = findViewById(R.id.inicio_sesion);
        mUserEditText = findViewById(R.id.correo);
        mPasswordEditText = findViewById(R.id.password);

        mEntrarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* validate form
                 if (it's ok) {
                */
                    Log.i("Validation", "Success validation");
                    CUser user = new CUser();
                    user.setUser(mUserEditText.getText().toString());
                    user.setPassword(mPasswordEditText.getText().toString());
                    presenter.doLogin(user);
                /*
                } else
                    --show error
                }
                */
            }
        });

    }

    // View implements

    @Override
    public void showError(CError error) {
        Log.e("Error", error.getMessage());
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void nextView() {
        Intent intent = new Intent(MainActivity.this, PanelActivity.class);
        startActivity(intent);
    }
}
